<?php
/**
 * Default Template
 *
 * Description: When loading the theme this is the default used for new pages.
 *
 * Author: Bas Grolleman <bgrolleman@emendo-it.nl>
 *
 */
wp_enqueue_style('home');
get_header(); 
?>

<div id="primary" class="site-content">
  <div id="content" role="main">
	  <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">   
        <?php render_block(get_the_block('topleft')); ?>    
      </div> <!-- leftTopPane -->
      <div id="leftBottomPane">
        <?php render_block(get_the_block('bottomleft')); ?> 
      </div> <!-- leftBottomPane -->
    </div> <!-- leftPane -->
    <div id="rightPane">
      <?php if (show_circular_arrows()) { ?> <div id="pijllr"></div> <?php } ?>
		  <div id="topPane">       
		    <?php render_block(get_the_block('topright')); ?>   
		  </div> <!-- topPane -->
      <div id="bottomPane">    
        <?php if (show_circular_arrows()) { ?> <div id="pijltb"></div> <?php } ?>
        <?php render_block(get_the_block('bottomright')); ?>
        <?php if (show_circular_arrows()) { ?> <div id="pijlrl"></div> <?php } ?>
      </div>  <!-- bottomPane -->
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

