<div id="socialbuttons">
 <ul>
	<li><a id="youtube" target="_blank" href="http://www.youtube.com/user/freekmosterman?feature=watch" title="Freek Mosterman YouTube"></a></li>
	<li><a id="linkedin" target="_blank" href="http://www.linkedin.com/in/freekmosterman" title="Freek Mosterman LinkedIn"></a></li>
	<li><a id="facebook" target="_blank" href="https://www.facebook.com/freek.mosterman" title="Freek Mosterman Facebook"></a></li>
	<li><a id="twitter" target="_blank" href="https://twitter.com/#!/freekmosterman" title="Freek Mosterman Twitter"></a></li>
 </ul>
</div>
