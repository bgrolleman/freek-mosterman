<?php
//$ss_dir = get_stylesheet_directory_uri();
//require 'querypath-3.0.0/src/qp.php';
require 'lib/functions.php';
require 'lib/display_images_in_list.php';
require 'lib/split_content.php';
require 'lib/display_video.php';
require 'lib/acf.php';

// [tweets twitterid="twitterid-value"]
require 'lib/tweets.php';

// Menu's
function theme_menu() {
  $menu_defaults = array(
    'container'  => false,
		'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
		'depth' => 2
  );
  print '<div id="menu">';
  wp_nav_menu($menu_defaults);
  print '</div>';
}

/* Load Scripts when not on the Admin Side */
if (!is_admin()) {
  // Load JQuery from Google 
  wp_deregister_script('jquery');
  wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . 
    "://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js", false, null);
  wp_enqueue_script('jquery');
	wp_register_script('imagerotator', get_stylesheet_directory_uri() . '/js/imagerotator.js',array('jquery'));
	wp_register_script('imagerotator_big', get_stylesheet_directory_uri() . '/js/imagerotator_big.js',array('jquery'));

  // CSS Style's with dependencies
  wp_register_style('imagerotator', get_stylesheet_directory_uri() . '/css/imagerotator.css');
  wp_register_style('home', get_stylesheet_directory_uri() . '/css/home.css', array('menu'));
	wp_register_style('buro', get_stylesheet_directory_uri() . '/css/buro.css', array('menu'));
  wp_register_style('projecten', get_stylesheet_directory_uri() . '/css/projecten.css', array('menu'));
  wp_register_style('contact', get_stylesheet_directory_uri() . '/css/contact.css', array('menu'));
  wp_register_style('werkwijze', get_stylesheet_directory_uri() . '/css/werkwijze.css', array('buro'));
  wp_register_style('extradiensten', get_stylesheet_directory_uri() . '/css/extradiensten.css', array('buro'));
  wp_register_style('exterieur', get_stylesheet_directory_uri() . '/css/exterieur.css', array('projecten'));
  wp_register_style('menu', get_stylesheet_directory_uri() . '/css/menu.css');
  wp_enqueue_style('menu');
}

?>
