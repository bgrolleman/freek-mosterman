$(document).ready(function() {
  new Mosterman.ImageRotator();
});

var Mosterman = {};

Mosterman.ImageRotator = function() {
	this.init();
	return this;
}

Mosterman.ImageRotator.prototype = {
	constructor: Mosterman.ImageRotator,
	
	carouselIndex: 0,
	numGalleries: 0,
	numImagesPerGallery: [],
	currentImageRotator: null,
	slideshowInterval: 0,
	currentSlideshowIndex: 1,
	
	init: function() {
		this.currentImageRotator = $("#imageRotator");
		
		$("#arrowLeft").click($.proxy(this.leftArrowClicked, this));
		$("#arrowRight").click($.proxy(this.rightArrowClicked, this));
		this.numGalleries = $(".imageRotator").length;
		
		var count = 0;
		var self = this;
		$(".imageRotator").each(function() {
			self.numImagesPerGallery.push($(this).children().children().length);
		});
		
		if(this.numImagesPerGallery.length > 0 && this.numImagesPerGallery[this.carouselIndex] > 2)
			this.slideshowInterval = window.setInterval($.proxy(this.slideshowNext, this), 3000);
	},
	
	leftArrowClicked: function() {
		if(this.carouselIndex == 0) return;
		console.log("left clicked");
		this.transitionToFrom(--this.carouselIndex, this.carouselIndex+1);
	},

	rightArrowClicked: function() {
		if(this.carouselIndex == this.numGalleries - 1) return;
		console.log("right clicked");
		this.transitionToFrom(++this.carouselIndex, this.carouselIndex-1);
	},
	
	transitionToFrom: function(iTo, iFrom) {
		window.clearInterval(this.slideshowInterval);
		iFrom++; iTo++;
		
		this.currentImageRotator = '#imageRotator';
		var prevRot = '#imageRotator';
		var self = this;
		
		$(prevRot).hide(500, function() {
			$(prevRot).animate( {'scrollLeft':0},1);
			$(self.currentImageRotator).show(500, function(){});
		});
		
		var nVid = '#video' + iTo;
		var pVid = '#video' + iFrom;
		var nCont = '#content' + iTo;
		var pCont = '#content' + iFrom;

		$(pVid).hide(500, function() { $(nVid).show(500); });
		$(pCont).fadeOut(1000);
		$(nCont).fadeIn(1000);
		
		this.currentSlideshowIndex = 0;
		if(this.numImagesPerGallery.length > 0 && this.numImagesPerGallery[this.carouselIndex] > 2)
			this.slideshowInterval = window.setInterval($.proxy(this.slideshowNext, this), 3000);
	},
	
	slideshowNext: function() {
		console.log(this);
		$(this.currentImageRotator).animate({'scrollLeft': 415 * this.currentSlideshowIndex++}, 500);
		var ci = this.currentSlideshowIndex;
		var mi = this.numImagesPerGallery[0];
		console.log(ci +">="+ mi);
		if(ci >= mi) {
			console.log("uberschnitzel");
			this.currentSlideshowIndex = 0;
		}
	}
};
