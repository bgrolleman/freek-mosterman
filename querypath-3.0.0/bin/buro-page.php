<?php
/**
 * Template Name: Buro Page Template
 *
 * Description: page template for static Buro page
 *
 * @package WordPress
 * @subpackage Freek
 * @since Freek 0.1.0
 */

get_header(); 
$ss_dir = get_stylesheet_directory_uri(); ?>

<div id="primary" class="site-content">
  <div id="content" role="main">
	  <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">   <?php render_block(get_the_block('topleft')); ?>   </div>
      <div id="leftBottomPane"><?php render_block(get_the_block('bottomleft')); ?></div> 
    </div> <!-- leftPane -->
    <div id="rightPane">
      <div id="topPane">       <?php render_block(get_the_block('topright')); ?>   </div> 
      <div id="bottomPane">    <?php render_block(get_the_block('bottomright')); ?></div> 
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>
