<?php
/**
 * Template Name: Buro Pagina
 *
 * Description: Layout voor Buro pagina
 *
 */
wp_enqueue_style('buro');
wp_enqueue_script('imagerotator');
get_header(); 
the_post();
?>

<div id="primary" class="site-content">
  <div id="content" role="main">
  <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">   
        <?php the_content() ?>
      </div> <!-- leftTopPane -->
      <div id="leftBottomPane">
        <div class="reference"> 
          <?= get_field('linkedin') ?>
        </div>
      </div> <!-- leftBottomPane -->
    </div> <!-- leftPane -->
    <div id="rightPane">
      <div id="topPane">       
        <?php get_template_part('part/imagerotator'); ?>
      </div> <!-- topPane -->
      <div id="bottomPane">    
        <?= get_field('algemene_voorwaarden'); ?>
      </div>  <!-- bottomPane -->
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

