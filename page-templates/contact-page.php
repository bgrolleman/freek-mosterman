<?php
/**
 * Template Name: Contact Pagina
 *
 * Description: Layout voor Contact pagina
 *
 */
wp_enqueue_style('contact');
wp_enqueue_script('imagerotator');
get_header(); 
the_post();
?>

<div id="primary" class="site-content">
  <div id="content" role="main">
	  <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">   
				<?php the_content() ?>
      </div> <!-- leftTopPane -->
      <div id="leftBottomPane">
				<div id="texttopleft"><p>Meer weten? Een vrijblijvend gesprek?<br/>Mail, bel, kom langs!</p></div>
				<?php get_template_part('part/social'); ?>
      </div> <!-- leftBottomPane -->
    </div> <!-- leftPane -->
    <div id="rightPane">
		  <div id="topPane">       
				<?php get_template_part('part/imagerotator'); ?>
		  </div> <!-- topPane -->
      <div id="bottomPane">    
					<div id="contactlogo" /></div>
					<div id="address1"><p>
						Postadres:<br/>
						<a target="_blank" href="https://maps.google.com/maps?q=Van+Tuyllstraat+38,+Hooglanderveen&hl=en&sll=52.186063,5.432954&sspn=0.023522,0.066047&hq=Van+Tuyllstraat+38,&hnear=Hooglanderveen,+Amersfoort,+Utrecht,+The+Netherlands&t=m&z=16">Van Tuyllstraat 38</a><br/>
						3829 AD  Hooglanderveen<br/>
						M 06 52420490
					</p></div>
          <div id="address2"><p>
            Bezoekadres:<br/>
            <a target="_blank" href="https://www.google.nl/maps/place/Ruimtesonde+9,+3824+MZ+Amersfoort/@52.2012352,5.3864924,17z/data=!3m1!4b1!4m2!3m1!1s0x47c64726a823d039:0xb37473c82a4403a">Ruimtesonde 9</a><br/>
            3824 MZ  AMERSFOORT<br/>
						E <a href="mailto:info@freekmosterman.nl">info@freekmosterman.nl</a>
					</p></div>
				</p>
      </div>  <!-- bottomPane -->
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

