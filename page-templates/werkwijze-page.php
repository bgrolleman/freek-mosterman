<?php
/**
 * Template Name: Werkwijze
 *
 * Description: Werkwijze pagina, 4 blokken met pijlen, content word gesplits op de "more" tag. 
 *
 */
wp_enqueue_style('werkwijze');
get_header(); 

// Only works on pages, getting the content
the_post();
// Split content on the more tag
$content = split_content();
?>

<div id="primary" class="site-content">
  <div id="content" role="main">
    <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">
				<?php if (array_key_exists(0,$content)) { echo $content[0]; }; ?>
      </div> <!-- leftTopPane -->
      <div id="leftBottomPane">
				<?php if (array_key_exists(3,$content)) { echo $content[3]; }; ?>
      </div> <!-- leftBottomPane -->
    </div> <!-- leftPane -->
    <div id="rightPane">
      <div id="pijllr"></div>
      <div id="topPane">
				<?php if (array_key_exists(1,$content)) { echo $content[1]; }; ?>
      </div> <!-- topPane -->
      <div id="bottomPane">    
        <div id="pijltb"></div>
        <div id="pijlrl"></div>
				<?php if (array_key_exists(2,$content)) { echo $content[2]; }; ?>
      </div>  <!-- bottomPane -->
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

