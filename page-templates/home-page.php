<?php
/**
 * Template Name: Startpagina
 *
 * Description: De standaard startpagina
 *
 */
wp_enqueue_style('home');
wp_enqueue_script('imagerotator');
get_header(); 

// Only works on pages, getting the content
the_post();
?>

<div id="primary" class="site-content">
  <div id="content" role="main">
    <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">
        <div id="introContent">
          <?php the_content(); ?>
        </div>
      </div> <!-- leftTopPane -->
      <div id="leftBottomPane">
				<?php get_template_part('part/social'); ?>
      </div> <!-- leftBottomPane -->
    </div> <!-- leftPane -->
    <div id="rightPane">
      <div id="topPane">
				<?php get_template_part('part/imagerotator'); ?>
      </div> <!-- topPane -->
      <div id="bottomPane">
				<?php echo do_shortcode('[tweets twitterid=freekmosterman]'); ?>
      </div>  <!-- bottomPane -->
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

