<?php
/**
 * Template Name: Project Page Template
 *
 * Description: page template for static Project pages
 *
 * @package WordPress
 * @subpackage Freek
 * @since Freek 0.1.0
 */

wp_enqueue_style('exterieur');
wp_enqueue_style('imagerotator');
wp_enqueue_script('imagerotator_big');
get_header(); 

$page_id = $post->ID;
$sibling_list = get_pages(array(
	'sort_column'=>'menu_order',
	'sort_order'=>'asc',
	'child_of'=>wp_get_post_parent_id($page_id),
	'post_type'=>get_post_type($page_id)
));

$pages = array();
foreach ($sibling_list as $sibling ) {
  $pages[] = $sibling->ID;
}

$current = array_search($page_id, $pages);
$prevID = isset($pages[$current-1]) ? $pages[$current -1] : false;
$nextID = isset($pages[$current+1]) ? $pages[$current +1] : false;

the_post();
$parent_id = $post->post_parent;
?>

<div id="primary" class="site-content">
<div id="content" role="main" class="<?php echo get_custom_field_value($parent_id,'menu'); ?>">
	  <?php theme_menu(); ?>    
		<div id="topPane">
			<div id="imageRotator1" class="imageRotator">
				<div id="innerRotator1" class="innerRotator">
					<?php display_images_in_list('full',false) ?>
				</div>  
			</div>
    </div> <!-- topPane -->
    
		<div id="bottomPane">
			<div id="leftPane">
				<div id="arrowLeft">
					<?php if ( $prevID ) { ?>
					<a href="<?= get_permalink($prevID); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrowleft.png" width="58px" height="97px"></a>
					<?php }; ?>
			  </div> <!-- arrowLeft -->
				<?php the_content(); ?>
			</div> <!-- leftPane -->
			
			<div id="rightPane">
				<div id="arrowRight">
          <?php if ( $nextID ) { ?>
					<a href="<?= get_permalink($nextID); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrowright.png" width="58px" height="97px"></a>
          <?php }; ?>
				</div> <!-- arrowRight -->
				<div>
					<?php echo display_video('406','285'); ?>
				</div> <!-- videoDiv -->
			</div> <!-- rightPane -->
		</div> <!-- bottomPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

