<?php
/**
 * Template Name: Extra Diensten
 *
 * Description: Extra Diensten Pagina
 *
 * Block TL: Content
 * Block TR: Static Image
 * Block BL: Static Image
 * Block BR: Rotater
 *
 * Special Fields:
 *   imagetr - Image URL for Top Right
 *   imagebl - Image URL for Bottom Left
 */
wp_enqueue_style('extradiensten');
wp_enqueue_script('imagerotator');
get_header(); 

// Only works on pages, getting the content
the_post();
$page_id   = get_queried_object_id();
?>

<div id="primary" class="site-content">
  <div id="content" role="main">
    <?php theme_menu(); ?>
    <div id="leftPane">
      <div id="leftTopPane">
				<?php the_content(); ?>
      </div> <!-- leftTopPane -->
      <div id="leftBottomPane">
				<!-- Single Image -->
        <img src="<?= get_field('afbeelding_linksonder') ?>">
      </div> <!-- leftBottomPane -->
    </div> <!-- leftPane -->
    <div id="rightPane">
      <div id="pijllr"></div>
      <div id="topPane">
				<?php get_template_part('part/imagerotator'); ?>
      </div> <!-- topPane -->
      <div id="bottomPane">    
        <div id="pijltb"></div>
        <div id="pijlrl"></div>
        <?php 
           $imagetr = get_custom_field_value($page_id,'imagetr'); 
           if ( $imagetr != '' ) { 
					   print '<img src="' . $imagetr . '/>';
					 } else {
						 print display_video('394','285'); 
           } ?>
      </div>  <!-- bottomPane -->
    </div> <!-- rightPane -->
  </div><!-- #content -->
</div><!-- #primary -->

<?php get_footer(); ?>

