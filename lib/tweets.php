<?php
add_shortcode( 'tweets', 'tweets_func' );

// [tweets twitterid="twitterid-value"]
function tweets_func( $atts ) {
	extract( shortcode_atts( array(
	  //freekmosterman
		'twitterid' => '',
	), $atts ) );

  $s = <<<END_STR
	<div id="twitterPane">
    <script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>
      <a class="twitter-timeline" 
         href="https://twitter.com/{$twitterid}" 
				 data-widget-id="378298503614566400"
         data-chrome="noborders transparent nofooter">
         Tweets by @{$twitterid}</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
  </div> <!-- twitterPane -->
END_STR;
  
	return $s;
}
?>
