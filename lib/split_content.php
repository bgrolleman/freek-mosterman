<?php
// Split Content on the <!-- more --> tags, to break text in blocks 

function split_content() {
  global $more;
	$more = true;
	$content = preg_split('/(<span id="more-\d+"><\/span>|<!--more-->)/i', get_the_content('more'));
	for($c = 0, $csize = count($content); $c < $csize; $c++) {
	  $content[$c] = apply_filters('the_content', $content[$c]);
	}
#	$content = array(get_the_content('more'));
	return $content;
}
?>
