<?php

function emit_class_selected_if_this_page($match_name) {
  emit_class_selected_if_page_name(get_this_page_name(), $match_name);
}

function emit_class_selected_if_page_name($page_name, $match_name) {
  if ($page_name == $match_name) { echo ' class="selected" '; }
}

function get_this_page_name() {
  return get_queried_object()->post_title;
}

function emit_page_specific_css() {
  $page_id   = get_queried_object_id();
  $css_group = get_custom_field_value($page_id,'css_group');
	wp_enqueue_style(strtolower($css_group));
}

function get_content_for_this_page_section($section_name) {
  get_content_for_page_section(get_queried_object_id(), $section_name);
}

function dbgmsg($name,$val) {
  echo " [--- {$name}: {$val} ---] ";
}

function get_post_img_urls($post_id) {
  $args = array( 'post_type'      => 'attachment', 
                 'numberposts'    => -1, 
                 'post_mime_type' => 'image', 
                 'post_status'    => null, 
                 'post_parent'    => $post_id ); 

  $urls = array();

  $attachments = get_posts($args);
  if ($attachments) {
    foreach ( $attachments as $attachment ) {
      // Method #1: Allows you to define the image size
      $src = wp_get_attachment_image_src( $attachment->ID, "attached-image");
      if ($src) {
        $urls[] = $src[0]; // element 0 is the url.
      }
    }
  }

  return $urls;
}

function get_content_for_page_section($page_id,$section_name) {
// problem:
// can't search by exact multi-category
// so instead we'll search by parent page name category (home, buro, etc.)
//   then iterate thru the results looking for the custom_field entry we want
//   ( "position" ).  

  // Use query with categories to fetch the content.
  // Depending on the content_type custom value, decide how to
  //   output the result.

  //echo " Page ID: {$page_id} // ";
  //echo " Section Name: {$section_name} // ";

  $page_obj  = get_page($page_id);
  $page_name = $page_obj->post_title;
  if ($page_name === '') { return; }

  $cat_obj = get_category_by_slug(strtolower($page_name)); 
  $cat_id = $cat_obj->term_id;
  //echo '<!-- page name: ' . $page_name . ' -->';
  $args      = array('posts_per_page' => -1, 'category' => $cat_id);
  //echo '<!-- category: ' . $args['category'] . ' -->';
  $posts     = get_posts( $args );
  //echo '<!-- number of matching posts: ' . count($posts) . ' -->';

  foreach ($posts as $p) {
    setup_postdata($p);
    $p_id    = $p->ID;
    $p_title = $p->post_title;
    $cfields = get_post_custom($p_id);

    if (array_key_exists('position', $cfields)) {
      $position_ar = $cfields['position'];

      if (count($position_ar) < 1) {
        // This would imply that someone forgot to add the custom_field "position".
        dbgmsg("count(position_ar) < 1...", count($position_ar));
        break;
      }

      if ($position_ar[0] !== $section_name) {
        // Not this post...
        continue;
      }

      if (array_key_exists('content_type', $cfields)) {
        $content_type_ar = $cfields['content_type'];
        if (count($content_type_ar) > 0) {
          //dbgmsg("content_type_ar[0]",$content_type_ar[0]);

          // based on the value of content_type, output the content appropriately
          switch ($content_type_ar[0]) {
          case 'text_html':
            //echo " XXXX we would output text_html here XXXX ";
            //echo apply_filters('the_content',$p->post_content);
            emit_text_html($p->post_content);
            break;
          case 'images_slideshow':
            //echo " XXXX we would output image_slideshow here XXXX ";
            emit_images_slideshow(get_post_img_urls($p_id));
            break;
          case 'image';
            emit_image(get_post_img_urls($p_id));
            break;
          } // end of switch
        } // end of if count of content_type_ar > 0
      } // end of if array key exists 'content_type'
    } // end of if array key exists 'position'
  } // end of foreach post

  wp_reset_postdata();
}

function emit_text_html($s) {
  echo apply_filters('the_content',$s);
}

function emit_image($url_ar) {
  $s .= '<img src="' . $url_ar[0] . '">';
  echo($s);
}

function emit_images_slideshow($url_ar) {
  $s = '<div id="imageRotator"><div id="innerRotator">';

  $image_count = 0;
  foreach ($url_ar as $url) {
    $s .= '<img src="' . $url . '">';
    $image_count += 1;
  }

  $s .= '</div></div>';

  echo($s);
  emit_slideshow_script($image_count);
}

function emit_slideshow_script($image_count, $target_div = "#imageRotator") {
  $s = "<script type='text/javascript'>
        var i = 1;
        function next() {
          $('{$target_div}').animate({'scrollLeft':415*i++},500);
          if (i == {$image_count}) { i = 0; }
        }
        window.setInterval('next()',3000)
      </script>
    ";
  echo($s);
}

// Get rid of the extra paragraph tags that WP adds.
// Disabled because this breaks the wysiwyg text editor - Bas 
// remove_filter( 'the_content', 'wpautop' );

function render_block($block_content) {
  $images  = qp($block_content, 'img');
  $anchors = qp($block_content, 'a');
  $paras   = qp($block_content, 'p');
  $divs    = qp($block_content, 'div');
  
  $n_images  = $images->size();
  $n_anchors = $anchors->size();
  $n_paras   = $paras->size();
  $n_divs    = $divs->size();
  
  if ($n_images > 0 
      && $n_anchors == $n_images 
      && $n_paras == 0 
      && $n_divs == 0) {
    if ($n_images == 1) {
      // single image output
      $urls = array();

      // This is ugly and could/should be cleaned up
      foreach ($images as $image) {
        $src = $image->attr('src');
        if ($src) {
          $urls[] = $src;
        }
        break; // we only need the first image
      } //end of foreach image
      emit_image($urls);
    }
    else {
      // slideshow
      $urls = array();

      foreach ($images as $image) {
        $src = $image->attr('src');
        if ($src) {
          $urls[] = $src;
        }
      } //end of foreach image
      
     emit_images_slideshow($urls); 
    } //end of else (n_images != 1)
  }
  else {
    // assume normal html output
    emit_text_html($block_content);
  }
}

function get_custom_field_value($page_id,$field_name) {
  $cfs = get_post_custom($page_id);
  $ar  = $cfs[$field_name];
  if (count($ar) > 0) {
    return $ar[0];
  }
  else {
    return '';
  }
}
  
function show_circular_arrows() {
  // Pages with custom field "show_circular_values" == 1 should show the red circular arrows.
  return (get_custom_field_value(get_queried_object_id(),'show_circular_arrows') == 1);
}


?>
