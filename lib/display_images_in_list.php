<?php
function display_images_in_list($size = "thumbnail", $random = true) {
  if($acf_images = get_field('project_afbeeldingen')) {
    if ( get_field('project_random') ) { shuffle($acf_images); }
    foreach( $acf_images as $image ) {
      if ( $size == 'full' ) {
        $imageurl = $image['url'];
      } else {
        $imageurl = $image['sizes'][$size];
      }
      print '<a rel="lightbox" href="' . $image["url"] . '"><img src="' . $imageurl . '" /></a>';
    }
  } else if($images = get_posts(array(
    'post_parent'    => get_the_ID(),
    'post_type'      => 'attachment',
    'numberposts'    => -1, // show all
    'post_status'    => null,
    'post_mime_type' => 'image',
    'orderby'        => 'title',
    'order'           => 'ASC',
  ))) {
		if ( $random ) {
			shuffle($images);
		}
    foreach($images as $image) {
			print '<a rel="lightbox" href="' . wp_get_attachment_url($image->ID) . '">' . wp_get_attachment_image($image->ID,$size) . '</a>';
    }
  }
}
?>
