<?php
function display_video($width, $height) {
  $yt = get_field('project_video');
  if ( $yt == '' ) {
    $yt = get_custom_field_value($page_id,'youtube');
  }

	if ( $yt == '' ) { 
		if($videos = get_posts(array(
			'post_parent'    => get_the_ID(),
			'post_type'      => 'attachment',
			'numberposts'    => -1, // show all
			'post_status'    => null,
			'post_mime_type' => 'video',
			'orderby'        => 'menu_order',
			'order'           => 'ASC',
		))) {
			$video_url = $videos[0]->guid;
			$video_type = $videos[0]->post_mime_type;
			return "
				<video width='${width}' height='${height}' controls='controls'>
					<source src='${video_url}' type='${video_type}' /e
				</video>
			";
		}
	} else {
		parse_str( parse_url( $yt, PHP_URL_QUERY ), $my_array_of_vars );
		$yid = $my_array_of_vars['v'];    
		echo '<iframe width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/' . $yid . '?rel=0&controls=1&showinfo=1&modestbranding=1"  frameborder="0" allowfullscreen></iframe>';
	}
}
?>
