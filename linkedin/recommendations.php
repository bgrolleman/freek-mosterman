<?php
	$divid = wp_generate_password(12, false);
  $length = 4096;
?>
<div class="linkedin">
		<div class="items" style="height: 255px; padding-right: 5px; overflow: auto; width: 400px;">
		<?php foreach ($recommendations as $recommendation): ?> 
			<blockquote>
				<div class="recommendation"><?php  nl2br(wp_linkedin_excerpt($recommendation->recommendationText, $length)); ?></div>
				<div class="recommender"><a href="<?php echo $recommendation->recommender->publicProfileUrl; ?>"
					target="_blank"><?php echo $recommendation->recommender->firstName; ?> <?php echo $recommendation->recommender->lastName; ?></a></div>
			</blockquote>
      <br/>
		<?php  endforeach; ?>
		</div>
</div>
