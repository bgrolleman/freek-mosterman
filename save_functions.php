<!-- BEGIN /wp-content/themes/twentytwelve-child/functions.php -->
<?php

function get_current_page_id() {
  $q_obj    = get_queried_object();
  $page_id  = get_queried_object_id();
  $page_obj = get_page($page_id);
  //echo $page_obj->post_name;
  //echo $page_id;
  return $page_id;
}

function get_content_for_this_page_section($section_name) {
  get_content_for_page_section(get_current_page_id(), $section_name);
}

function dbgmsg($name,$val) {
  echo " [--- {$name}: {$val} ---] ";
}

function get_content_for_page_section($page_id,$section_name) {
// problem:
// can't search by exact multi-category
// so instead we'll search by parent page name category (home, buro, etc.)
//   then iterate thru the results looking for the custom_field entry we want
//   ( "position" ).  

  // Use query with categories to fetch the content.
  // Depending on the content_type custom value, decide how to
  //   output the result.

  //echo " Page ID: {$page_id} // ";
  //echo " Section Name: {$section_name} // ";

  $page_obj  = get_page($page_id);
  $page_name = $page_obj->post_name;
  $args      = array('posts_per_page' => -1, 'category' => $page_name);
  $posts     = get_posts( $args );

  dbgmsg("count(posts)",count($posts));
  $n_iter = 0;

  foreach ($posts as $p) {
    $n_iter += 1;
    //dbgmsg("n_iter",$n_iter);
    $p_id    = $p->ID;
    $p_title = $p->post_title;
    //dbgmsg("p_id",$p_id);
    //dbgmsg("p_title",$p_title);
    $cfields = get_post_custom($p_id);

    if (array_key_exists('position', $cfields)) {
      $pos_ar = $cfields['position'];

      dbgmsg("count(pos_ar)",count(pos_ar));
      switch (count(pos_ar)) {
      case 0:
        dbgmsg("pos_ar[] is empty","");
        break;
      case 1:
        dbgmsg("pos_ar[0]",$pos_ar[0]);
        break;
      case 2:
        dbgmsg("pos_ar[1]",$pos_ar[1]);
        break;
      case 3:
        dbgmsg("pos_ar[2]",$pos_ar[2]);
        break;
      default:
        dbgmsg("pos_ar[] has more than 3 elements","");
      }
      dbgmsg("section_name",$section_name);

      if (count($pos_ar) < 1) {
        echo "AAAAAAA";
        break;
      }

      if ($pos_ar[0] !== $section_name) {
        echo "BBBBBBB";
        continue;
      }

      if (array_key_exists('content_type', $cfields)) {
        $content_type_ar = $cfields['content_type'];
        if (count($content_type_ar) > 0) {
          echo "XX content_type = {$content_type_ar[0]}";

          // based on the value of content_type, output the content appropriately
          switch ($content_type_ar[0]) {
          case 'text_html':
            echo " XXXX we would output text_html here XXXX ";
            echo apply_filters('the_content',$p->post_content);
            break;
          case 'images_slideshow':
            echo " XXXX we would output image_slideshow here XXXX ";
            print_r($p);
            break;
          } // end of switch
        } // end of if count of content_type_ar > 0
      } // end of if array key exists 'content_type'
    } // end of if array key exists 'position'
  } // end of foreach post

  wp_reset_postdata();
}

?>
<!-- END /wp-content/themes/twentytwelve-child/functions.php -->
